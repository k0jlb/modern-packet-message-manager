import pytest
from mpmm.models.message import Message, MessageDirection, MessageType

MOCK_MESSAGE_EXAMPLE = {
        "sender":"MOCKSENDER",
        "receiver":"MOCKRECEIVER",
        "content":"The Quick Brown Fox Jumped Over The Lazy Dog.",
        "type": MessageType.PACKET,
        "direction": MessageDirection.INCOMING
    }

def test_message():
    """
    Happy-path test to confirm base assumptions about Message class
    """
    # Arrange
    MOCK_MESSAGE_DATA = MOCK_MESSAGE_EXAMPLE

    # Act
    obj = Message(**MOCK_MESSAGE_DATA)

    # Assert
    assert isinstance(obj, Message)
    assert obj.immutable is True
    assert obj.created_ts
    assert obj.updated_ts
    assert obj.created_ts == obj.updated_ts


REQUIRED_MESSAGE_FIELDS = [
    "sender",
    "receiver",
    "content",
    "type",
    "direction"
    ]
@pytest.mark.parametrize("field",REQUIRED_MESSAGE_FIELDS)
def test_message_required_fields(field):
    """
    Parameterized test to check that message creation fails when required field is missing

    Args:
        field (string): Field name to test
    """
    # Arrange
    MOCK_MESSAGE_DATA = MOCK_MESSAGE_EXAMPLE
    MOCK_MESSAGE_DATA.pop(field)

    # Act
    with pytest.raises(Exception):
        obj = Message(**MOCK_MESSAGE_DATA)
