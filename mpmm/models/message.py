from datetime import date, datetime
from enum import Enum

from pydantic import BaseModel


class MessageType(Enum):
    BBS = 1
    PACKET = 2
    APRS = 3
    WINLINK = 4

class MessageDirection(Enum):
    INCOMING = 1
    OUTGOING = 2

class Message(BaseModel):
    receiver: str
    sender: str
    content: str
    type: MessageType
    direction: MessageDirection
    created_ts: datetime = datetime.now()
    updated_ts: datetime = created_ts
    immutable: bool = True
