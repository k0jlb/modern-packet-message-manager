from ctypes import addressof
from enum import Enum
from typing import List
from pydantic import BaseModel, Field, validator
from typing import Optional
from models.message import MessageType
from abc import ABC, abstractmethod
import re

class ConnectionType(Enum):
    TCPKISS = 1
    SERIAL = 2
    TELNET = 3
    AGWPE = 4

class BaseTNCConfig(BaseModel, ABC):
    connection: ConnectionType
    name: str
    description: Optional[str]
    configuration: dict = {}
    address: str
    kiss_capable: bool = False

class TCPKissTNCConfig(BaseTNCConfig):
    connection: ConnectionType = Field(ConnectionType.TCPKISS, const=True)
    kiss_capable: bool = Field(True, const=True)
    
    @validator('address')
    def address_must_be_valid(value):
        VALIDATOR_REX = r'[\d\w\.\-]+:\d+'
        assert re.match(VALIDATOR_REX, value), 'Address must be a valid network address'
        return value