from models.message import Message
from typing import List, Type, TypeVar
from pydantic import BaseModel
from abc import ABC, abstractclassmethod
from logging import getLogger

from models.general_config import ConfigBase

logger = getLogger('mpmm_models_state')

T = TypeVar('T', bound='PersistentState')


class PersistentState(BaseModel, ABC):
    messages: List[Message] = []
    config: ConfigBase = ConfigBase()

    @abstractclassmethod
    def persist_state(cls, state: Type[T]):
        pass

    def persist(self):
        self.__class__.persist_state(self)

    @abstractclassmethod
    def load_state(cls: Type[T]) -> T:
        pass


class FilePersistentState(PersistentState):
    filename: str = 'state.json'
    
    def persist(self):
        self.__class__.persist_state(self, self.filename)

    @classmethod
    def persist_state(cls, state: PersistentState, filename='state.json'):
        with open(filename, 'w') as statefile:
            statefile.write(state.json())

    @classmethod
    def load_state(cls: Type[T], filename="state.json") -> T:
        state = cls.parse_file(filename)
        return state