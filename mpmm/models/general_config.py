from typing import List
from pydantic import BaseModel
from models.tnc_config import BaseTNCConfig


class GeneralConfig(BaseModel):

    callsign: str = "" #TODO: Add class/validation for callsign
    location: str = ""
    grid_locator: str = "" #TODO: Add class/validation for grid locator


class ConfigBase(BaseModel):
    general_config: GeneralConfig = GeneralConfig()
    tnc_config: List[BaseTNCConfig] = []
