from typing import List, Union
from functools import wraps
from api.state import persistent_state
from models.general_config import ConfigBase, GeneralConfig
from fastapi import APIRouter, Path, HTTPException

from models.tnc_config import BaseTNCConfig, TCPKissTNCConfig

config_router = APIRouter(
    prefix = '/config'
)

def triggers_persist(func):
    @wraps(func)
    def persist_wrapper(*args, **kwargs):
        return_value = func(*args, **kwargs)
        persistent_state.persist()
        return return_value
    return persist_wrapper

@config_router.get("/", response_model=ConfigBase, tags=['Configuration'])
def get_configuration() -> ConfigBase:
    return persistent_state.config


###### General Configuration Items ######
@config_router.get("/general", response_model=GeneralConfig, tags=['Configuration - General'])
def get_general_configuration() -> GeneralConfig:
    return persistent_state.config.general_config

@config_router.patch("/general", response_model=GeneralConfig, tags=['Configuration - General'])
@triggers_persist
def set_general_configuration(config: GeneralConfig) -> GeneralConfig:
    updated_config = config.dict(exclude_unset=True, exclude_defaults=True)
    persistent_state.config.general_config = persistent_state.config.general_config.copy(update=updated_config)
    return persistent_state.config.general_config

###### TNC Configuration Items ######
@config_router.get('/tnc', response_model=List[Union[TCPKissTNCConfig]], tags=['Configuration - TNC'])
def get_tnc_list() -> List[Union[TCPKissTNCConfig]]:
    return persistent_state.config.tnc_config

@config_router.get('/tnc/{index}', response_model=Union[TCPKissTNCConfig], tags=['Configuration - TNC'])
def get_single_tnc(index: int = Path(title="Index of the TNC configuration to show")) -> Union[TCPKissTNCConfig]:
    try:
        return persistent_state.config.tnc_config[index]
    except IndexError:
        raise HTTPException(status_code=400, detail=f"TNC Configuration with index {index} does not exist.")

@config_router.post('/tnc', response_model=Union[TCPKissTNCConfig], tags=['Configuration - TNC'])
@triggers_persist
def add_tnc(tnc_config: Union[TCPKissTNCConfig]) -> Union[TCPKissTNCConfig]:
    persistent_state.config.tnc_config.append(tnc_config)
    return tnc_config

@config_router.patch('/tnc/{index}', response_model=Union[TCPKissTNCConfig], tags=['Configuration - TNC'])
@triggers_persist
def update_tnc(tnc_config: Union[TCPKissTNCConfig], index: int) -> Union[TCPKissTNCConfig]:
    try:
        current_tnc = persistent_state.config.tnc_config[index]
        updated_values = tnc_config.dict(exclude_unset=True, exclude_defaults=True)
        new_tnc = current_tnc.copy(update=updated_values)
        persistent_state.config.tnc_config[index] = new_tnc
        return new_tnc
    except IndexError:
        raise HTTPException(status_code=400, detail=f"TNC Configuration with index {index} does not exist.")

