from models.state import FilePersistentState
from logging import getLogger

logger = getLogger()

try:
    persistent_state = FilePersistentState.load_state(filename="state.json")
except Exception as raised_exception:
    logger.exception("Problem loading state")
    persistent_state = FilePersistentState()