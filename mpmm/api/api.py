from sys import exc_info
from fastapi import FastAPI
from api.state import persistent_state
from logging import getLogger
from models.message import Message, MessageDirection, MessageType
from api.base_config import config_router
from tnc.tcpkisstnc import TCPKissTNC

api_app = FastAPI(title="Modern Packet Message Manager - API")

api_app.include_router(config_router)

@api_app.get("/state")
def api_root():
    return persistent_state.dict()


@api_app.get("/test")
def test_kiss():
    cfg = persistent_state.config.tnc_config[0]
    tnc = TCPKissTNC(cfg)
    tnc.test()