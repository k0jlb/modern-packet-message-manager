from imp import reload
from fastapi import FastAPI, Request
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles
import uvicorn
from models.message import Message
from api.api import api_app

main_app = FastAPI(title="Modern Packet Message Manager")

main_app.mount("/api", api_app)
main_app.mount("/", StaticFiles(directory="ui", html=True), name="ui")

if __name__ == "__main__":
    print("Starting MPMM on http://localhost:80")
    uvicorn.run("mpmm:main_app", host="0.0.0.0", port=80, log_level="info")