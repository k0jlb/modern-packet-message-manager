from models.tnc_config import TCPKissTNCConfig
import kiss

class TCPKissTNC(object):
    configuration: TCPKissTNCConfig
    network_address: str
    network_port: int
    tnc_port: kiss.KISS


    def __init__(self, configuration: TCPKissTNCConfig) -> None:
        self.configuration = configuration
        address_components = self.configuration.address.split(':')
        self.network_address = address_components[0]
        self.network_port = int(address_components[1])


    def test(self) -> bool:
        self.tnc_port = kiss.TCPKISS(self.network_address, self.network_port)
        self.tnc_port.start()
        return True